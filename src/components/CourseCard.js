import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';


export default function CourseCard({course}) {

  const {name, description, price, _id} = course;
  // Use the state hook for this component to be able to store its state
  // States are used to keep track of information related to individual components
  /*
    SYNTAX
    const [getter, setter] = useState(initialGetterValue);
  */

  // const [count, setCount] = useState(0);
  // const [seat, setSeat] = useState(30);
  // const [isOpen, setIsOpen] = useState(true);

  // function enroll() {
  //   // if(seat > 0) {
  //   //   setCount(count + 1);
  //   //   setSeat(seat - 1); 
  //   // } else {
  //   //   alert('No more seats available');
  //   // }
  //     setCount(count + 1);
  //     setSeat(seat - 1); 
  // };

  // // useEffect() - will allow us to execute a function if the value of seats state changes
  // useEffect(() => {
  //   if(seat === 0) {
  //     setIsOpen(false);
  //     document.querySelector(`#btn-enroll-${_id}`).setAttribute('disabled', true);
  //   }
  //   // will run anytime if one of the values in the array of the dependencies changes
  // }, [seat])

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>
          Description:
        </Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>
          Price:
        </Card.Subtitle>
        <Card.Text>
          Php {price}
        </Card.Text>
{/*        <Card.Text>
          Enrollees: {count}
        </Card.Text>*/}
        {/*<Button id={`btn-enroll-${_id}`} variant="primary" onClick={enroll}>Enroll</Button>*/}
        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  );
}
