import { Row, Col, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
export default function Banner({data}) {
    const {title, route, label, content}  = data;
    console.log(title)
return (
    <Row>
    	<Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>
            <Button as={NavLink} to={route} variant="primary">{label}</Button>
        </Col>
    </Row>
	)
};