import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Register() {

    const navigate = useNavigate();

    const { user } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);

    // Function to simulate user registration
    function registerUser(e) {
        e.preventDefault()

        //  Clear input fields
        setEmail('');
        setPassword1(''); 
        setPassword2(''); 

        // alert('Thank you for registering')
        // Check Email
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data) {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                });
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if(data !== null){
                Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "Welcome to Zuitt."
                });
                navigate("/login");
                    } else {
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        })
                    }
                })
            }
        })
    }

    useEffect(() => {
        // Validation to enable submit button when all fields populated and both passwords match
        if(((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2) && (password1 === password2)) && mobileNo.length >= 11) {
            setIsActive(true);
            document.querySelector('.submit-btn').removeAttribute('disabled');
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNo, email, password1, password2]);

    return (
        <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group controlId="userFirstName">
            <Form.Label>First Name:</Form.Label>
            <Form.Control 
            type="text" 
            placeholder="First Name"
            value={firstName}
            onChange={e => setFirstName(e.target.value)} 
            required
            />
        </Form.Group>
        <Form.Group controlId="userLastName">
            <Form.Label>Last Name:</Form.Label>
            <Form.Control 
            type="text" 
            placeholder="Last Name"
            value={lastName}
            onChange={e => setLastName(e.target.value)} 
            required
            />
        </Form.Group>
        <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
            type="email" 
            placeholder="Enter email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
            />
            <Form.Text className="text-muted">
            We'll never share your email with anyone else.
            </Form.Text>
        </Form.Group>
        <Form.Group controlId="userMobileNo">
            <Form.Label>Mobile Number:</Form.Label>
            <Form.Control 
            type="number" 
            placeholder="Mobile No."
            value={mobileNo}
            onChange={e => setMobileNo(e.target.value)} 
            required
            />
        </Form.Group>
        <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Password"
        value={password1}
        onChange={e => setPassword1(e.target.value)} 
        required
        />
        </Form.Group>

        <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Verify Password"
        value={password2}
        onChange={e => setPassword2(e.target.value)} 
        required
        />
        </Form.Group>

        <Button className="mt-3 submit-btn" variant={isActive ? 'primary' : 'danger'} type="submit" id="submitBtn" disabled>
        Submit
        </Button>
        </Form>
        )

}