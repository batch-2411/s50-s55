import { useState, useEffect } from 'react';

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data);
		})
	}, [])


	// const courses = coursesData;
	return (
		<>
		{/*Props Drilling - we can pass information from one component to another using props*/}
		{/*{} - used in props to signify that we are providing information*/}
			{
				courses.map(course => <CourseCard key={course._id} course = {course}/>)
			}
		</>
	)
}