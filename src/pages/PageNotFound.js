import Banner from '../components/Banner';

export default function PageNotFound() {
	    const data = {
	    title: "Zuitt Coding Bootcamp",
        content: "Page Not Found",
        route:"/",
        label: "Homepage"
    }
return (
    <Banner data={data}/>
	)
};