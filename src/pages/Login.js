import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);

    // Returns a function that lets you navigate to components
    // const navigate = useNavigate();

    // Function to simulate user registration
    function loginUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user information is found, the "access" property will not be available and return undefined

            if(typeof data.access !== "undefined"){
                // The JWT will be used ton retrieve user information accross the whole frontend application and storing it in the localStorage
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
                } else {
                    Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        });

        // alert('You are now logged in.')
        // localStorage.setItem('email', email);
        // Sets the global user state to have properties obtained from local storage
        // setUser({email: localStorage.getItem('email')});
        // navigate('/')
        //  Clear input fields
        setEmail('');
        setPassword(''); 
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            // Global user state for validation accross the whole app
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across whole application
            setUser({
                id: data.   id,
                isAdmin: data.isAdmin
            })
        })
    };

    // useEffect(() => {
    //     // Validation to enable submit button when all fields populated
    //     if(email !== '' && password !== '' ) {
    //         setIsActive(true);
    //     } else {
    //         setIsActive(false);
    //     }
    // }, [email, password]);

    return (
        (user.id !== null) ?
        <Navigate to = "/courses"/>
        :
        <Form onSubmit={(e) => loginUser(e)}>
        	<h1>Login</h1>	
            <Form.Group controlId="userEmail" className="mb-3">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password" className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)} 
	                required
                />
            </Form.Group>
            <Button variant={isActive ? 'success' : 'danger'} type="submit" id="submitBtn">
                Login
            </Button>
        </Form>
    )

}