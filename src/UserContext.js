import React from 'react';

// "React.createContext" - is a function in the React library that creates a new context object.
// "Context Object" - is a data type of an object that can use to store information and can be shared to other components within the app.
const UserContext = React.createContext();

// "UserContext.Provider" is a component that is created when you use React.createContext().
// "Provider" component that allows other components to use the context object  and supply necessary information needed to tje context object
export const UserProvider = UserContext.Provider;

export default UserContext;